from transformers import pipeline
# import tensorflow as tf
import torch
import pandas as pd
from tqdm.notebook import tqdm
pd.options.mode.chained_assignment = None
from transformers import BertTokenizer
from torch.utils.data import TensorDataset
from transformers import BertForSequenceClassification
 
def hatespeech(article): 
    # device_name = tf.test.gpu_device_name()
    # if device_name != '/device:GPU:0':
    #     raise SystemError('GPU device not found')

    # print('Found GPU at: {}'.format(device_name))
    
    
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', do_lower_case=True)
    
    
    
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
    model = BertForSequenceClassification.from_pretrained(
        './bert-model-hate-speech'
    )
    
    
    
    Pipeline = pipeline('sentiment-analysis', model=model, tokenizer=tokenizer,device = 0)
    
    text_to_predict = article
    
    Pipeline.__call__(text_to_predict)


hatespeech("hello")