from flask import Flask, request, jsonify
from flask import render_template, redirect
from flask_cors import CORS, cross_origin
from hatespeech_model import hatespeech   #file is the name of the python file which runs the model on the article string
 
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

 
 
@app.route('/')
@cross_origin()
def home():
    #return render_template("landing.html")  # sample 
    return "Nescii ML backend"
 
@app.route('/hatespeech',methods=['POST'])
@cross_origin()
def hate_speech():
    article = request.form['article']
    # print(request.form['article'])
    # print(article)
    # return "test"
    return 'Not NSFW' if hatespeech(article)==0 else 'NSFW'  
 
 
 
 
 
if __name__ == "__main__":
    app.run(debug=True, use_reloader=False)